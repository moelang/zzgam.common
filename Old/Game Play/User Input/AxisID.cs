﻿using ZZgam.UnityBaseTools.Data_Type;

namespace ZZgam.UnityBaseTools.Game_Play.User_Input
{
    class AxisID : HeritableEnum<AxisID>
    {
        public AxisID(string name) : base(name) { }
        private AxisID(string name, int value) : base(name, value) { }

        public static AxisID Horizontal = new AxisID("Horizontal");
        public static AxisID Vertical = new AxisID("Vertical");
        public static AxisID Height = new AxisID("Height");//高度轴，并不存在于默认设置中
    }
}
