﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Action;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Func;

namespace ZZgam.UnityBaseTools.Game_Play.User_Input
{
    /// <summary>
    /// 输入轴绑定
    /// </summary>
    class BindingKeys : SpeakerSingleton<BindingKeys>
    {
        Dictionary<KeyCode, HashSet<Action<bool>>> dicKeys = new Dictionary<KeyCode, HashSet<Action<bool>>>();
        Dictionary<KeyCode, bool> dicKeyStates = new Dictionary<KeyCode, bool>();

        public ZZAction UpdateKeys = new ZZAction();

        public ZZFunc<KeyCode, Action<bool>, bool> BindKeyAction = new ZZFunc<KeyCode, Action<bool>, bool>();
        public ZZFunc<KeyCode, Action<bool>, bool> UnbindKeyAction = new ZZFunc<KeyCode, Action<bool>, bool>();

        protected override void SetupListeners(bool add)
        {
            UpdateKeys.AutoListener(add, InvokeUpdateKeys);
            BindKeyAction.AutoListener(add, InvokeBindKeyAction);
            UnbindKeyAction.AutoListener(add, InvokeUnbindKeyAction);
        }

        private void InvokeUpdateKeys()
        {
            foreach (var item in dicKeys)
            {
                bool state = Input.GetKey(item.Key);
                if (dicKeyStates[item.Key]!= state)
                {
                    dicKeyStates[item.Key] = state;
                    foreach (var action in item.Value)
                    {
                        action.Invoke(state);
                    }
                }
            }
        }

        public bool InvokeBindKeyAction(KeyCode keyCode, Action<bool> action)
        {
            if (dicKeys.ContainsKey(keyCode))
            {
                if (!dicKeys[keyCode].Contains(action))
                {
                    return dicKeys[keyCode].Add(action);
                }
                else
                {
                    Debug.LogError(action.ToString() + "已经存在于绑定列表中");
                }
            }
            else
            {
                try
                {
                    dicKeys.Add(keyCode, new HashSet<Action<bool>>() { action });

                    dicKeyStates.Add(keyCode, Input.GetKey(keyCode));

                    InputRecieve.UpdateKeysEnable = true;

                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }
            return false;
        }

        public bool InvokeUnbindKeyAction(KeyCode keyCode, Action<bool> action)
        {
            if (dicKeys.ContainsKey(keyCode))
            {
                if (dicKeys[keyCode].Contains(action))
                {
                    bool ret = dicKeys[keyCode].Remove(action);
                    if (dicKeys[keyCode].Count == 0)
                    {
                        dicKeys.Remove(keyCode);

                        dicKeyStates.Remove(keyCode);

                        if (dicKeys.Count == 0)
                        {
                            InputRecieve.UpdateKeysEnable = false;
                        }
                    }
                    return ret;
                }
                else
                {
                    Debug.LogErrorFormat("Action<bool>:{0}不存在!", action.ToString());
                }
            }
            else
            {
                Debug.LogErrorFormat("keyCode:{0}不存在!", keyCode.ToString());
            }
            return false;
        }
    }
}
