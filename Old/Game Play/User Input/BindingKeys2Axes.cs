﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ZZgam.UnityBaseTools.Data_Type;
using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Func;

namespace ZZgam.UnityBaseTools.Game_Play.User_Input
{
    /// <summary>
    /// 正反按键绑定
    /// 轴向输入
    /// </summary>
    class BindingKeys2Axes : SpeakerSingleton<BindingKeys2Axes>
    {
        public ZZFunc<KeyCode, KeyCode, AxisID, bool> BindKeys2Axes = new ZZFunc<KeyCode, KeyCode, AxisID, bool>();
        public ZZFunc<AxisID, bool> UnbindKeys2Axes = new ZZFunc<AxisID, bool>();
        public ZZFunc<AxisID, Action<float>, bool> BindAxisAction = new ZZFunc<AxisID, Action<float>, bool>();
        public ZZFunc<AxisID, Action<float>, bool> UnbindAxisAction = new ZZFunc<AxisID, Action<float>, bool>();

        class KeyPair : ZZPair<KeyCode, KeyCode>
        {
            KeyCode keyPos;
            KeyCode keyNeg;
            bool keyPosPressed;
            bool keyNegPressed;
            AxisID axisID;
            BindingKeys2Axes keys2Axes;
            public KeyPair(KeyCode keyPos, KeyCode keyNeg, AxisID axisID, BindingKeys2Axes keys2Axes) : base(keyPos, keyNeg)
            {
                if (keyPos == keyNeg)
                {
                    Debug.LogErrorFormat("无法使用两个相同的按键{1}:{2}", keyPos, keyNeg);
                }
                this.keyPos = keyPos;
                this.keyNeg = keyNeg;
                this.axisID = axisID;
                this.keys2Axes = keys2Axes;
                keyPosPressed = Input.GetKey(keyPos);
                keyNegPressed = Input.GetKey(keyNeg);
                BindingKeys.Instance.BindKeyAction.Invoke(keyPos, PostiveKeyPressed);
                BindingKeys.Instance.BindKeyAction.Invoke(keyNeg, NegativeKeyPressed);
            }

            ~KeyPair()
            {
                BindingKeys.Instance.UnbindKeyAction.Invoke(keyPos, PostiveKeyPressed);
                BindingKeys.Instance.UnbindKeyAction.Invoke(keyNeg, NegativeKeyPressed);
            }

            internal void PostiveKeyPressed(bool pressed)
            {
                keyPosPressed = pressed;
                UpdateAxisAction();
            }

            internal void NegativeKeyPressed(bool pressed)
            {
                keyNegPressed = pressed;
                UpdateAxisAction();
            }

            private void UpdateAxisAction()
            {
                foreach (var item in keys2Axes.dicAxes)
                {
                    if (item.Key == axisID)
                    {
                        float axisValue = (keyPosPressed ? 1 : 0) + (keyNegPressed ? -1 : 0);
                        foreach (var action in item.Value)
                        {
                            action.Invoke(axisValue);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 虚拟轴与按键对映射关系字典
        /// </summary>
        Dictionary<AxisID, KeyPair> dicVirtualAxes = new Dictionary<AxisID, KeyPair>();
        /// <summary>
        /// 虚拟轴与响应事件回调对应
        /// </summary>
        Dictionary<AxisID, HashSet<Action<float>>> dicAxes = new Dictionary<AxisID, HashSet<Action<float>>>();

        protected override void SetupListeners(bool add)
        {
            BindKeys2Axes.AutoListener(add, InvokeBindKeys2Axes);
            UnbindKeys2Axes.AutoListener(add, InvokeUnbindKeys2Axes);
            BindAxisAction.AutoListener(add, InvokeBindAxisAction);
            UnbindAxisAction.AutoListener(add, InvokeUnbindAxisAction);
        }

        private bool InvokeBindKeys2Axes(KeyCode keyPos, KeyCode keyNeg, AxisID axisID)
        {
            if (dicVirtualAxes.ContainsKey(axisID))
            {
                Debug.LogErrorFormat("虚拟轴{0}已经绑定到{1}:{2}", axisID, dicVirtualAxes[axisID].t1, dicVirtualAxes[axisID].t2);
            }
            else
            {
                try
                {
                    KeyPair virtualAxis = new KeyPair(keyPos, keyNeg, axisID, this);
                    dicVirtualAxes.Add(axisID, virtualAxis);
                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }
            return false;
        }

        private bool InvokeUnbindKeys2Axes(AxisID axisID)
        {
            if (dicVirtualAxes.ContainsKey(axisID))
            {
                try
                {
                    dicVirtualAxes.Remove(axisID);
                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }
            else
            {
                Debug.LogErrorFormat("虚拟轴{0}尚未绑定", axisID);
            }
            return false;
        }

        public bool InvokeBindAxisAction(AxisID axisID, Action<float> action)
        {
            if (dicAxes.ContainsKey(axisID))
            {
                if (!dicAxes[axisID].Contains(action))
                {
                    return dicAxes[axisID].Add(action);
                }
                else
                {
                    Debug.LogError(action.ToString() + "已经存在于绑定列表中");
                }
            }
            else
            {
                try
                {
                    dicAxes.Add(axisID, new HashSet<Action<float>>() { action });

                    InputRecieve.UpdateAxesEnable = true;

                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }
            return false;
        }

        public bool InvokeUnbindAxisAction(AxisID axisID, Action<float> action)
        {
            if (dicAxes.ContainsKey(axisID))
            {
                if (dicAxes[axisID].Contains(action))
                {
                    bool ret = dicAxes[axisID].Remove(action);
                    if (dicAxes[axisID].Count == 0)
                    {
                        dicAxes.Remove(axisID);
                        if (dicAxes.Count == 0)
                        {
                            InputRecieve.UpdateAxesEnable = false;
                        }
                    }
                    return ret;
                }
                else
                {
                    Debug.LogErrorFormat("ZZAction<float>:{0}不存在!", action.ToString());
                }
            }
            else
            {
                Debug.LogErrorFormat("AxisID:{0}不存在!", axisID.ToString());
            }
            return false;
        }
    }
}
