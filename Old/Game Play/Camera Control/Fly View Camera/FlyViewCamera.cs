﻿using UnityEngine;
using ZZgam.UnityBaseTools.Game_Play.User_Input;

/// <summary>
/// 飞行模式相机
/// 巡视检查场景
/// </summary>
[RequireComponent (typeof (Camera))]
class FlyViewCamera : MonoBehaviour {
    public float moveSpeed = 1;

    float moveHorizontal = 0;
    float moveVertical = 0;
    float moveHeight = 0;

    void CameraMoveH (float axis) {
        moveHorizontal = axis;
    }

    void CameraMoveV (float axis) {
        moveVertical = axis;
    }

    void CameraMoveHeight (float axis) {
        moveHeight = axis;
    }

    void Start () {
        BindingAxes.Instance.BindAxisAction.Invoke (AxisID.Horizontal, CameraMoveH);
        BindingAxes.Instance.BindAxisAction.Invoke (AxisID.Vertical, CameraMoveV);

        BindingKeys2Axes.Instance.BindKeys2Axes.Invoke (KeyCode.X, KeyCode.Z, AxisID.Height);
        BindingKeys2Axes.Instance.BindAxisAction.Invoke (AxisID.Height, CameraMoveHeight);
    }

    void OnDestroy () {
        BindingAxes.Instance.UnbindAxisAction.Invoke (AxisID.Horizontal, CameraMoveH);
        BindingAxes.Instance.UnbindAxisAction.Invoke (AxisID.Vertical, CameraMoveV);

        BindingKeys2Axes.Instance.UnbindKeys2Axes.Invoke (AxisID.Height);
        BindingKeys2Axes.Instance.UnbindAxisAction.Invoke (AxisID.Height, CameraMoveHeight);
    }

    void LateUpdate () {
        transform.Translate (moveHorizontal * moveSpeed * Time.deltaTime, moveHeight * moveSpeed * Time.deltaTime, moveVertical * moveSpeed * Time.deltaTime);
    }
}