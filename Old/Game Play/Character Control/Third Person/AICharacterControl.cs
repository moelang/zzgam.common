﻿using UnityEngine;
using UnityEngine.AI;

namespace ZZgam.UnityBaseTools.Game_Play.Character_Control.Third_Person
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        public NavMeshAgent Agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for


        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            Agent = GetComponentInChildren<NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

            Agent.updateRotation = false;
            Agent.updatePosition = true;
        }


        private void Update()
        {
            if (target != null)
                Agent.SetDestination(target.position);

            if (Agent.remainingDistance > Agent.stoppingDistance)
                character.Move(Agent.desiredVelocity, false, false);
            else
                character.Move(Vector3.zero, false, false);
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }
    }
}
