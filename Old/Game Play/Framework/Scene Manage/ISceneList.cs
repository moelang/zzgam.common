﻿namespace ZZgam.UnityBaseTools.Game_Play.Framework.Scene_Manage
{
    /// <summary>
    /// 场景列表接口
    /// </summary>
    public interface ISceneList
    {
        /// <summary>
        /// 获取对应场景脚本对象
        /// </summary>
        /// <param name="scriptID">脚本枚举值</param>
        /// <returns>场景脚本对象</returns>
        ISceneScript GetSceneMonoScript(SceneID sceneID);
    }
}
