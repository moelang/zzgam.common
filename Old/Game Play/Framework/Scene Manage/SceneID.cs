﻿using ZZgam.UnityBaseTools.Data_Type;

namespace ZZgam.UnityBaseTools.Game_Play.Framework.Scene_Manage
{
    public sealed class SceneID : HeritableEnum<SceneID>
    {
        ISceneScript script;

        private SceneID(string name) : base(name) { }
        private SceneID(string name, int value) : base(name, value) { }

        public SceneID(ISceneScript script) : base(script.SceneName)
        {
            this.script = script;
        }

        public SceneID(ISceneScript script, int value) : base(script.SceneName, value)
        {
            this.script = script;
        }

        public ISceneScript GetSceneScript()
        {
            return script;
        }
    }
}
