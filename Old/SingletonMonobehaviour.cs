﻿using System.Collections.Generic;
using UnityEngine;

namespace ZZgam.UnityBaseTools
{
    public abstract class SingletonMonobehaviour<T> : MonoBehaviour where T : SingletonMonobehaviour<T>
    {
        static List<GameObject> listGO = new List<GameObject>();

        static readonly object syslock = new object();

        static bool applicationIsQuitting = false;

        static T instance;

        public static T Instance
        {
            get
            {
                if (applicationIsQuitting)
                {
                    return null;
                }
                lock (syslock)
                {
                    if (instance == null)
                    {
                        if (listGO.Count > 0)
                        {
                            Debug.LogErrorFormat("{0}中instance为空，但listGO不为0，是否有其他线程正在操作或之前创建失败?", typeof(T).Name);
                            instance = listGO[0].GetComponent<T>();
                        }
                        else
                        {
                            instance = FindObjectOfType<T>();
                            if (!instance)
                            {
                                instance = new GameObject(typeof(T).Name).AddComponent<T>();
                            }
                            listGO.Add(instance.gameObject);
                        }
                    }
                    return instance;
                }
            }
        }

        protected virtual void Awake()
        {
            if (instance == null)
            {
                instance = GetComponent<T>();
            }
            SetupListeners(true);
        }

        protected virtual void OnDestroy()
        {
            SetupListeners(false);
            listGO.Clear();
            instance = null;
        }

        protected virtual void OnApplicationQuit()
        {
            applicationIsQuitting = true;
        }

        protected virtual void SetupListeners(bool add) { }
    }
}
