﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Events;

namespace Packages.zzgam.common
{
    class EditorModeChaged : MonoBehaviour
    {
#if UNITY_EDITOR
        public UnityEvent OnExitPlayMode;
        private void Awake()
        {
            EditorApplication.playModeStateChanged += Changed;
        }

        private void Changed(PlayModeStateChange obj)
        {
            switch (obj)
            {
                case PlayModeStateChange.EnteredEditMode:
                    break;
                case PlayModeStateChange.ExitingEditMode:
                    break;
                case PlayModeStateChange.EnteredPlayMode:
                    break;
                case PlayModeStateChange.ExitingPlayMode:
                    OnExitPlayMode?.Invoke();
                    break;
                default:
                    break;
            }
        }
    }
#endif
}
