﻿using System;

namespace ZZgam.UnityBaseTools.Data_Type
{
    /// <summary>
    /// 成对结构1
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    class ZZPair<T1, T2> : IEquatable<ZZPair<T1, T2>>
    {
        public T1 t1;
        public T2 t2;

        public ZZPair(T1 t1, T2 t2)
        {
            this.t1 = t1;
            this.t2 = t2;
        }

        public bool Equals(ZZPair<T1, T2> other)
        {
            return t1.Equals(other.t1) && t2.Equals(other.t2);
        }
    }
}
