﻿using UnityEngine;
using UnityEngine.XR;

namespace ZZgam.UnityBaseTools {
    class CameraFly : MonoBehaviour {
        public float moveSpeed = 1;

        public float rotation_H_speed = 1;
        public float rotation_V_speed = 1;
        public float max_up_angle = 80;
        public float max_down_angle = -60;

        float current_rotation_H;
        float current_rotation_V;
        Camera[] cameras;
        bool enableSimulate;
        private void Start () {
            cameras = GetComponentsInChildren<Camera> ();
            enableSimulate = !XRSettings.enabled;
        }
        void LateUpdate () {
            if (enableSimulate) {
                current_rotation_H += Input.GetAxis ("Mouse X") * rotation_H_speed;
                current_rotation_V += Input.GetAxis ("Mouse Y") * rotation_V_speed;
                current_rotation_V = Mathf.Clamp (current_rotation_V, max_down_angle, max_up_angle);
                cameras[0].transform.localEulerAngles = new Vector3 (-current_rotation_V, current_rotation_H, 0f);
                cameras[0].transform.Translate (new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical")) * Time.deltaTime * moveSpeed);
                foreach (var item in cameras) {
                    item.transform.localPosition = cameras[0].transform.localPosition;
                    item.transform.localRotation = cameras[0].transform.localRotation;
                }
            }
        }
    }
}