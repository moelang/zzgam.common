﻿using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Assets.ZZ.Scenes.Loaders.Level
{
    public class MB_Level : SingletonBehaviour<MB_Level, BM_Level>
    {
        protected override void SetupListeners(bool add)
        {
            BM_Level.Instance.ELoadLevel.AutoListener(add, LoadLevel);
            BM_Level.Instance.EUnloadLevel.AutoListener(add, UnloadLevel);
            BM_Level.Instance.EIsLevelLoaded.AutoListener(add, IsLevelLoaded);
        }

        Scene currentScene;

        private void LoadLevel(string arg0, BM_Level.LevelLoadModel? arg1)
        {
            StartCoroutine(LoadSceneByName(arg0, arg1));
        }

        private void UnloadLevel(string arg0)
        {
            StartCoroutine(UnloadSceneByName(arg0));
        }

        private bool IsLevelLoaded(string t1)
        {
            return SceneManager.GetSceneByName(t1).isLoaded;
        }

        private IEnumerator LoadSceneByName(string arg0, BM_Level.LevelLoadModel? arg1 = BM_Level.LevelLoadModel.AddThisAndReleaseActive)
        {
            switch (arg1)
            {
                default:
                case BM_Level.LevelLoadModel.AddThisAndReleaseActive:
                    if (currentScene.isLoaded && !currentScene.name.Equals(gameObject.name))
                    {
                        yield return SceneManager.UnloadSceneAsync(currentScene);
                    }
                    yield return SceneManager.LoadSceneAsync(arg0, LoadSceneMode.Additive);
                    currentScene = SceneManager.GetSceneByName(arg0);
                    SceneManager.SetActiveScene(currentScene);
                    BM_Level.Instance.ELevelLoaded.Invoke(arg0, BM_Level.LevelLoadModel.AddThisAndReleaseActive);
                    break;
                case BM_Level.LevelLoadModel.AddThisNotActive:
                    yield return SceneManager.LoadSceneAsync(arg0, LoadSceneMode.Additive);
                    BM_Level.Instance.ELevelLoaded.Invoke(arg0, BM_Level.LevelLoadModel.AddThisNotActive);
                    break;
            }
        }

        private IEnumerator UnloadSceneByName(string arg0)
        {
            var s = SceneManager.GetSceneByName(arg0);
            if (s.isLoaded)
            {
                yield return SceneManager.UnloadSceneAsync(s.name);
            }
        }
    }
}
