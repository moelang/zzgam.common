﻿using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Action;

namespace Assets.ZZ.Scenes.Loaders.Script
{
    public class BM_Script : SingletonManager<BM_Script, MB_Script>
    {
        public ZZAction<string> ELoadAutoSingleScript = new ZZAction<string>();
        public ZZAction<string> EAutoSingleScriptLoaded = new ZZAction<string>();
        public ZZAction<string> ELoadScript = new ZZAction<string>();
        public ZZAction<string> EScriptLoaded = new ZZAction<string>();
        public ZZAction<string> EUnloadScript = new ZZAction<string>();
        public ZZAction<string> EScriptUnloaded = new ZZAction<string>();
    }
}
