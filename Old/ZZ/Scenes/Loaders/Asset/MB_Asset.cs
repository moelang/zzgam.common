﻿using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using UnityEngine;

namespace Assets.ZZ.Scenes.Loaders.Asset
{
    public class MB_Asset : SingletonBehaviour<MB_Asset, BM_Asset>
    {
        protected override void SetupListeners(bool add)
        {
            BM_Asset.Instance.ELoadResource_AudioClip.AutoListener(add, ELoadResource_AudioClip);
        }

        private AudioClip ELoadResource_AudioClip(string t1)
        {
            return Resources.Load<AudioClip>(t1);
        }
    }
}
