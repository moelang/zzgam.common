﻿using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using System;
using System.Collections;
using System.IO.Ports;
using UnityEngine;

namespace Assets.ZZ.Utils.Devices
{
    public class MB_SP : SingletonBehaviour<MB_SP, BM_SP>
    {
        protected override void SetupListeners(bool add)
        {
            BM_SP.Instance.EOpenPort.AutoListener(add, OpenPort);
            BM_SP.Instance.EClosePort.AutoListener(add, ClosePort);
            BM_SP.Instance.EStartRead.AutoListener(add, StartRead);
            BM_SP.Instance.EStopRead.AutoListener(add, StopRead);
        }

        private void OnApplicationQuit()
        {
            StopRead();
        }

        bool isAlive = true;
        SerialPort sp = null;

        private void OpenPort(SerialPortConfig arg0)
        {
            sp = new SerialPort("\\\\.\\" + arg0.portName, arg0.baudRate, arg0.parity, arg0.dataBits, arg0.stopBits)
            {
                ReadTimeout = arg0.timeOut
            };
            try
            {
                sp.Open();
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        private void ClosePort()
        {
            StopRead();
            if (sp != null)
            {
                try
                {
                    sp.Close();
                    sp = null;
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex.Message);
                }
            }
        }

        private void StartRead(int arg0)
        {
            StartCoroutine(DataReceiveFunction(arg0));
        }

        private void StopRead()
        {
            isAlive = false;
        }

        IEnumerator DataReceiveFunction(int length)
        {
            byte[] dataBytes = new byte[length];
            byte[] dataPiker = new byte[length];
            int pickedLength = 0;
            isAlive = true;
            while (isAlive)
            {
                if (sp != null && sp.IsOpen)
                {
                    try
                    {
                        int readLen = sp.Read(dataBytes, 0, dataBytes.Length);
                        if (readLen < length)
                        {
                            if (pickedLength + readLen == length)
                            {
                                Array.Copy(dataBytes, 0, dataPiker, pickedLength, readLen);
                                BM_SP.Instance.EDataRead.Invoke(dataPiker);
                                pickedLength = 0;
                            }
                            else if (pickedLength + readLen < length)
                            {
                                Array.Copy(dataBytes, 0, dataPiker, pickedLength, readLen);
                                pickedLength += readLen;
                            }
                            else
                            {
                                int copyLen = length - pickedLength;
                                Array.Copy(dataBytes, 0, dataPiker, pickedLength, copyLen);
                                BM_SP.Instance.EDataRead.Invoke(dataPiker);
                                Array.Copy(dataBytes, copyLen, dataPiker, 0, readLen - copyLen);
                                pickedLength = readLen - copyLen;
                            }
                        }
                        else
                        {
                            pickedLength = 0;
                            BM_SP.Instance.EDataRead.Invoke(dataBytes);
                        }
                    }
                    catch (TimeoutException) { }
                    catch (Exception ex)
                    {
                        Debug.LogError(ex.Message);
                    }
                    yield return null;
                }
                else
                {
                    Debug.Log("Serial port exit");
                    yield break;
                }
            }
            ClosePort();
        }
    }
}
