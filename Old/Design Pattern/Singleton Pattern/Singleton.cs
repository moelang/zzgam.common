﻿namespace ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern
{
    public class Singleton<T> where T : class, new()
    {
        static T instance;
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new T();
                }
                return instance;
            }
        }
    }

    public class Singleton_Lock<T> where T : class, new()
    {
        static T instance;
        static readonly object _syslock = new object();
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (_syslock)
                    {
                        if (instance == null)
                        {
                            instance = new T();
                        }
                    }
                }
                return instance;
            }
        }
    }
}
