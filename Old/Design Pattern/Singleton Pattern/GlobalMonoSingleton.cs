﻿namespace ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern
{
    public class GlobalMonoSingleton<T> : MonoSingleton<T> where T : GlobalMonoSingleton<T>
    {
        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
        }
    }
}
