﻿using UnityEngine;

namespace ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern
{
    public class SingletonBehaviour<TSingletonBehaviour, TSingletonManager> : MonoBehaviour
        where TSingletonBehaviour : SingletonBehaviour<TSingletonBehaviour, TSingletonManager>
        where TSingletonManager : SingletonManager<TSingletonManager, TSingletonBehaviour>, new()
    {
        protected virtual void Awake()
        {
            SingletonManager<TSingletonManager, TSingletonBehaviour>.Instance.BindBehaviour(this);
            SetupListeners(true);
        }

        protected virtual void OnDestroy()
        {
            SetupListeners(false);
        }

        protected virtual void SetupListeners(bool add) { }
    }
}
