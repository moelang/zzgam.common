﻿using ZZgam.UnityBaseTools.Data_Type;

namespace ZZgam.UnityBaseTools.Design_Pattern.State_Pattern
{
    public class Transition : HeritableEnum<Transition>
    {
        protected Transition(string name) : base(name) { }
        protected Transition(string name, int value) : base(name, value) { }

        public static Transition NullTransition = new Transition("NullTransition", 0);
    }
}
