﻿using System;
using System.Collections;
using UnityEngine;

namespace ZZgam.UnityBaseTools.Utils
{
    public class Delay : SingletonGlobalMonobehaviour<Delay>
    {
        public void DoNextFrame(Action action)
        {
            StartCoroutine(CoroutineDoNextFrame(action));
        }

        public void DoAfterFrame(Action action, int frame)
        {
            StartCoroutine(CoroutineDoAfterFrame(action, frame));
        }

        public void DoNowOrTryNextFrame(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch
            {
                CoroutineDoNextFrame(action);
            }
        }

        private IEnumerator CoroutineDoNextFrame(Action action)
        {
            yield return null;
            if (action != null)
            {
                SafeInvoke(action);
            }
        }

        private IEnumerator CoroutineDoAfterFrame(Action action, int frame)
        {
            for (int i = 0; i < frame; i++)
            {
                yield return null;
            }
            if (action != null)
            {
                SafeInvoke(action);
            }
        }

        private void SafeInvoke(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
            }
        }

        public void DoAfterSeconds(float seconds, params Action[] actions)
        {
            StartCoroutine(CoroutineDoAfterSeconds(seconds, actions));
        }

        private IEnumerator CoroutineDoAfterSeconds(float seconds, params Action[] actions)
        {
            yield return new WaitForSeconds(seconds);
            if (actions != null)
            {
                foreach (var action in actions)
                {
                    SafeInvoke(action);
                }
            }
        }
    }
}
