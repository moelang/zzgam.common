﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.ZZgam.Utils
{
    class ShowFPS : MonoBehaviour
    {
        public Rect rect = new Rect(0, 0, 50, 25);

        Queue<float> lf = new Queue<float>();
        float fps = 0;
        private void Update()
        {
            lf.Enqueue(Time.deltaTime);
            if (lf.Count > 30)
            {
                lf.Dequeue();
                fps = 0;
                foreach (var item in lf)
                {
                    fps += item;
                }
                fps = 30 / fps;
            }
        }

        private void OnGUI()
        {
            GUI.Label(rect, string.Format("{0}:{1:F2}",Time.realtimeSinceStartup, fps));
        }
    }
}
