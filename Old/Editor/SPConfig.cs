﻿using System.IO;
using UnityEditor;
using UnityEngine;
using UE = UnityEditor.Editor;

namespace Assets.ZZ.Utils.Devices.Editor
{
    public class SPConfig : UE
    {
        //[MenuItem("ZZ/Utils/Devices/SPConfig/WriteDefaultConfig")]
        static void SetCurrentWorkScene()
        {
            SerialPortConfig defaultConfig = new SerialPortConfig("COM9");
            using (StreamWriter sw = new StreamWriter(Application.streamingAssetsPath + "/spconfig.json"))
            {
                sw.Write(EditorJsonUtility.ToJson(defaultConfig));
            };
        }
    }
}
