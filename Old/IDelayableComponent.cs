﻿namespace ZZgam.UnityBaseTools
{
    /// <summary>
    /// 待拓展
    /// 可延迟操作的组件接口，弱化组件脚本执行顺序带来的影响
    /// 当组件未初始化获取为null时，
    /// 其他脚本进行Get会自动fake一个临时对象并将对该对象的操作记入列表
    /// 当初始化完成后用正式对象替换fake并依次进行列表中操作
    /// </summary>
    interface IDelayableComponent
    {
    }
}
