﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Packages.zzgam.common.Component.Network.NetSocket
{
    public abstract class SocketPeel : IDisposable
    {
        protected Socket serverSocket;
        protected Socket socket;

        public SocketPeel()
        {
            serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        ~SocketPeel()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (socket.Connected)
            {
                socket.Disconnect(false);
            }
            socket.Dispose();

            if (serverSocket != null)
            {
                if (serverSocket.Connected)
                {
                    serverSocket.Disconnect(false);
                }
                serverSocket.Dispose();
            }
        }

        protected void Listen(IPAddress iPAddress, int port, int backlog)
        {
            serverSocket.Bind(new IPEndPoint(iPAddress, port));
            serverSocket.Listen(backlog);
            Task.Run(StartListen);
        }

        async Task StartListen()
        {
            while (true)
            {
                socket = await serverSocket.AcceptAsync();
                RecieveLoop(256);
            }
        }

        void RecieveLoop(int size_buf)
        {
            byte[] buffer = new byte[size_buf];
            List<byte> temp = new List<byte>();
            while (true)
            {
                int len = socket.Receive(buffer);
                if (len == 0)
                {
                    break;
                }
                var data = new byte[len];
                Array.Copy(buffer, data, len);
                temp.AddRange(data);
                while (temp.Count >= 4)
                {
                    int size = BitConverter.ToInt32(temp.GetRange(0, 4).ToArray(), 0);
                    if (size <= temp.Count - 4)
                    {
                        OnRecieved(temp.GetRange(4, size).ToArray());
                        temp = temp.GetRange(size + 4, temp.Count - size - 4);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        protected void Connect(IPAddress iPAddress, int port)
        {
            Task.Run(async () => { await socket.ConnectAsync(new IPEndPoint(iPAddress, port)); RecieveLoop(4096); });
        }


        public void Send(string data)
        {
            if (socket.Connected)
            {
                var buffer = Encoding.UTF8.GetBytes(data).ToList();
                var size = BitConverter.GetBytes(buffer.Count).ToList();
                size.AddRange(buffer);
                Task.Run(() => WaitSend(new ArraySegment<byte>(size.ToArray())));
            }
        }

        public void Send(byte[] data)
        {
            if (socket.Connected)
            {
                var buffer = data.ToList();
                var size = BitConverter.GetBytes(buffer.Count).ToList();
                size.AddRange(buffer);
                Task.Run(() => WaitSend(new ArraySegment<byte>(size.ToArray())));
            }
        }

        async Task<int> WaitSend(ArraySegment<byte> buffer, SocketFlags socketFlags = SocketFlags.None)
        {
            return await socket.SendAsync(buffer, socketFlags);
        }

        protected abstract void OnRecieved(byte[] data);
    }
}
