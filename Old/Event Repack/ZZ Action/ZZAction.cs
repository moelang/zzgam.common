﻿using System;
using ZZgam.UnityBaseTools.Event_Repack.Interface.Event_Listener;

namespace ZZgam.UnityBaseTools.Event_Repack.ZZ_Action
{
    interface IZZAction<TAction> : IAddListener<TAction>, IRemoveListener<TAction>, IAutoListener<TAction>
    {
        void RemoveAllListeners();
    }

    public class ZZAction : IZZAction<Action>
    {
        event Action ThisEvent;

        public void AddListener(Action action)
        {
            ThisEvent += action;
        }

        public void RemoveListener(Action action)
        {
            ThisEvent -= action;
        }

        public void AutoListener(bool add, Action action)
        {
            if (add)
            {
                AddListener(action);
            }
            else
            {
                RemoveListener(action);
            }
        }

        public void RemoveAllListeners()
        {
            ThisEvent = null;
        }

        public void Invoke()
        {
            if (ThisEvent != null)
            {
                ThisEvent.Invoke();
            }
        }
    }
    public class ZZAction<T> : IZZAction<Action<T>>
    {
        event Action<T> ThisEvent;

        public void AddListener(Action<T> action)
        {
            ThisEvent += action;
        }

        public void RemoveListener(Action<T> action)
        {
            ThisEvent -= action;
        }

        public void AutoListener(bool add, Action<T> action)
        {
            if (add)
            {
                AddListener(action);
            }
            else
            {
                RemoveListener(action);
            }
        }

        public void RemoveAllListeners()
        {
            ThisEvent = null;
        }

        public void Invoke(T t)
        {
            if (ThisEvent != null)
            {
                ThisEvent.Invoke(t);
            }
        }
    }
    public class ZZAction<T1, T2> : IZZAction<Action<T1, T2>>
    {
        event Action<T1, T2> ThisEvent;

        public void AddListener(Action<T1, T2> action)
        {
            ThisEvent += action;
        }

        public void RemoveListener(Action<T1, T2> action)
        {
            ThisEvent -= action;
        }

        public void AutoListener(bool add, Action<T1, T2> action)
        {
            if (add)
            {
                AddListener(action);
            }
            else
            {
                RemoveListener(action);
            }
        }

        public void RemoveAllListeners()
        {
            ThisEvent = null;
        }

        public void Invoke(T1 t1, T2 t2)
        {
            if (ThisEvent != null)
            {
                ThisEvent.Invoke(t1, t2);
            }
        }
    }
    public class ZZAction<T1, T2, T3> : IZZAction<Action<T1, T2, T3>>
    {
        event Action<T1, T2, T3> ThisEvent;

        public void AddListener(Action<T1, T2, T3> action)
        {
            ThisEvent += action;
        }

        public void RemoveListener(Action<T1, T2, T3> action)
        {
            ThisEvent -= action;
        }

        public void AutoListener(bool add, Action<T1, T2, T3> action)
        {
            if (add)
            {
                AddListener(action);
            }
            else
            {
                RemoveListener(action);
            }
        }

        public void RemoveAllListeners()
        {
            ThisEvent = null;
        }

        public void Invoke(T1 t1, T2 t2, T3 t3)
        {
            if (ThisEvent != null)
            {
                ThisEvent.Invoke(t1, t2, t3);
            }
        }
    }
}
