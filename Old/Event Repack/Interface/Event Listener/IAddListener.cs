﻿namespace ZZgam.UnityBaseTools.Event_Repack.Interface.Event_Listener
{
    interface IAddListener<T>
    {
        void AddListener(T listener);
    }
}
